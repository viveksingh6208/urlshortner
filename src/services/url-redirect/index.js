const { knex } = require("../../../config/db");
const {safePromise}= require('../../../utils')


const redirect = async function (shorturl) {

    const [err, data] = await safePromise(
        knex('urlmap')
        .select('long_url', 'is_secured', "id", 'clicks_count')
        .where({
            short_url: shorturl,
            active: 1
        })
    )
    
    

    if (!data.length || err) {
        
        return Promise.reject("requested url not found");
    }
    const issecured = data[0]["is_secured"]
    const urlMapId = data[0]['id']
    const clicksCount = data[0]['clicks_count'] + 1
    if (issecured) {
        const [err2] = await safePromise(
            knex('urlmap')
            .update({
                active: 0,
                clicks_count: clicksCount
            })
            .where({
            id: urlMapId
            })
        );

        if (err2) {
            throw err2;
        }
    } else {
        const [err2] = await safePromise(
            knex('urlmap')
            .update({
                clicks_count: clicksCount
            })
            .where({
                id: urlMapId
            })
        );

        if (err2) {
            throw err2;
        }

    }
    const redirectUrl = data[0]["long_url"];
    return (redirectUrl);


}
module.exports = redirect;