const urlHash = require('./url-hash')
const redirect = require('./url-redirect')

module.exports = {
    urlHash,
    redirect
}