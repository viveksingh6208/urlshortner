const { knex } = require('../../../config/db');
const { randomToken ,safePromise} =  require('../../../utils');
const base62 = require("base62/lib/ascii");
const { DOMAIN_NAME }= require('../../../constants')



async function urlHash(payload) {
    const { long_url,is_secured } = payload;
    let random;
    let randomurl;
    let short_url;
    const [err, data] = await safePromise(
        knex('urlmap')
        .select('short_url')
        .where({
            long_url
        })
    );

    if (err) {
        throw err;
    }
    if (data[0]) {
        short_url = DOMAIN_NAME + "/" + data[0]["short_url"]
    } else {
        random = randomToken();
        randomurl = base62.encode(random);
        short_url = DOMAIN_NAME + "/" + randomurl;
    }


    const [err1, data1] = await safePromise(
        knex('urlmap')
        .insert({
            long_url,
            random,
            short_url: randomurl,
            is_secured
        })
    );
    if (err1) {
        Promise.reject(err1);
    }

    return short_url;


}

module.exports = urlHash;