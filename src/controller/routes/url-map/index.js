var express = require('express');
const req = require('express/lib/request');
var router = express.Router();
const {
  knex
} = require("../../../../config/db");
const {
  safePromise
} = require('../../../../utils')
var base62 = require("base62/lib/ascii");
const { urlHash } = require('../../../services')




router.post('/url-short', async function (req, res) {
  const payload = req.body
  const [err, data] = await safePromise(urlHash(payload))
  if (err) {
    return res.status(500).json({
        "status": "fail",
        message: err,
    });
    }
    res.json({
      status: "sucess",
      message: "here is your short url",
      data
     
    });


})




module.exports = router;