const indexRouter = require('./main');
const urlRouter = require('./url-map');
const redirectRouter = require('./url-redirect');




module.exports = {
  routes: function(app, { MOUNT_POINT = '' }){
    // console.log(MOUNT_POINT);
    if(/^\/$/.test(MOUNT_POINT)){
      MOUNT_POINT = '';
    }
    app.use(`${MOUNT_POINT}/`, indexRouter);
    app.use(`${MOUNT_POINT}/`, urlRouter);
    app.use(`${MOUNT_POINT}/`, redirectRouter);
  }
}
