var express = require('express');
const req = require('express/lib/request');
var router = express.Router();
const { knex } = require("../../../../config/db");
const {safePromise} = require('../../../../utils');
const {redirect} = require('../../../services')


router.get('/:short_url', async (req, res) => {
    const short = req.params.short_url
    const [err, data] = await safePromise(redirect(short));
    if (err) {
        return res.status(500).json({
            "status": "fail",
            message: err,
        });
    }
    res.redirect(data);
})
module.exports = router;
