const config = require('./')




const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: config.DB_HOST,
    port: config.DB_PORT,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    database: config.DATABASE,
  }
});

module.exports = {
  knex
}