function safePromise(dbinfo) {
  return dbinfo
    .then(function (data) {
      return [null, data];
    })
    .catch(function (err) {
      return [err];
    })
}
module.exports = safePromise;