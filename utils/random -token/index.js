const randomToken = require("random-web-token");
const { TOKEN_FORMAT, TOKEN_LENGHT } = require('../../constants')

module.exports = () => randomToken.generate(TOKEN_FORMAT, TOKEN_LENGHT);