# url shortner 
## Api Documentation
### 1 api 
 http://ec2-54-173-208-114.compute-1.amazonaws.com:5000/url-short
-it is a post request
-takes 2 parameters in the body
-1st parameter is long_url 
-2nd parameter is is_secured
-response will be the link of short url
- curl:
`
curl --location --request POST 'http://ec2-54-173-208-114.compute-1.amazonaws.com:5000/url-short' \
--header 'Content-Type: application/json' \
--data-raw '{
    "long_url" : "https://github.com/nvm-sh/nvm#suppressing-colorized-output",
    "is_secured": 0
}'
`
### 2nd api
-it is a get request
-end point will be the response 1st api i.e shorturl
-hitting the api will redirect to the original long url
-curl:
`
curl --location --request GET 'http://ec2-54-173-208-114.compute-1.amazonaws.com:5000/j8LS'
`
