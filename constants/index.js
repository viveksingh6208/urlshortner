const DEFAULT = null;
const TOKEN_FORMAT = "onlyNumbers";
const TOKEN_LENGHT = 7;
const DOMAIN_NAME = "http://localhost:3000";



module.exports = {
    DEFAULT,
    TOKEN_FORMAT,
    TOKEN_LENGHT,
    DOMAIN_NAME
}